/**
 * Licensed Materials - Property of ReqPro.com & SmarterProcess - Copyright ReqPro 2020
 * Contact information: info@reqpro.com | info@smarterprocess.pl
 *
 *
 */

var sourceLinkType;
var direction;
var artRef;
var debug = false;
var succeeses;
var failures;

async function coverLinks() {
  succeeses = 0;
  failures = 0;
  var sourceType;
  if (direction == 0) {
    sourceType = new RM.LinkTypeDefinition(sourceLinkType, RM.Data.LinkDirection.IN);
  } else if (direction == 0) {
    sourceType = new RM.LinkTypeDefinition(sourceLinkType, RM.Data.LinkDirection.OUT);
  } else {
    sourceType = new RM.LinkTypeDefinition(sourceLinkType, RM.Data.LinkDirection.BIDIRECTIONAL);
  }

  var directionText = "_OBJ";
  if (direction == 1) directionText = "_SUB";
  else if (direction == 2) directionText = "_BI";
  else directionText = "_OBJ";

  for (var i = 0; i < artRef.length; i++) {
    var result = await getLinksForArtifact(artRef[i]);
    if (result.code == RM.OperationResult.OPERATION_OK) {
      var artLinks = result.data.artifactLinks;
      console.log(result.data.artifactLinks);
      if (artLinks.length > 0) {
        console.log("----");
        console.log(sourceLinkType);
        console.log(directionText);
        console.log("----");
        for (var link = 0; link < artLinks.length; link++) {
          console.log("***");
          console.log(artLinks[link].linktype.uri);
          console.log(artLinks[link].linktype.direction);
          console.log("***");
          if (artLinks[link].linktype.uri == sourceLinkType && artLinks[link].linktype.direction == directionText) {
            for (var z = 0; z < artLinks[link].targets.length; z++) {
              var tarRef = artLinks[link].targets[z];
              var souRef = artLinks[link].art;
              console.log("---");
              console.log(direction);
              if (direction != 2) {
                var resultDelete = await deleteLink(souRef, sourceType, tarRef);
                if (resultDelete.code == RM.OperationResult.OPERATION_OK) {
                } else {
                  console.log(resultDelete.code);
                  console.log(resultDelete.message);
                  failures = failures + 1;
                }
              } else {
                var resultDelete1;
                var resultDelete2;
                resultDelete1 = await deleteLink(souRef, sourceType, tarRef);
                resultDelete2 = await deleteLink(tarRef, sourceType, souRef);
                if (
                  resultDelete1.code == RM.OperationResult.OPERATION_OK ||
                  resultDelete2.code == RM.OperationResult.OPERATION_OK
                ) {
                } else {
                  failures = failures + 1;
                }
              }
              $(".status").html(
                `<b>Message:</b> Deleting links for <b> ${i + 1}/${artRef.length}</b> artifacts.
              <p><b>Errors:</b> Number of errors ${failures}</p>
              <p>Please do not perform any actions until process is finished.</p>`
              );
              gadgets.window.adjustHeight();
            }
          } else {
            console.log("Kicha");
          }
        }
      }
    }
  }

  $(".status").removeClass("warning correct incorrect");
  clearSelectedInfo();
  $("#actionButton").removeAttr("disabled");
  $(".status").addClass("correct");
  $(".status").html(`<b> Message:</b> Proccessed finished.
  <p><b> Status:</b> Links deleted successfully, errors ${failures}.</p>
  `);
  gadgets.window.adjustHeight();
}

function getLinksForArtifact(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getLinkedArtifacts(ref, resolve);
  });
}

function deleteLink(ref, linkType, tarRef) {
  return new Promise(function (resolve, reject) {
    RM.Data.deleteLink(ref, linkType, tarRef, resolve);
  });
}

$(function () {
  if (window.RM) {
    gadgets.window.adjustHeight();
    var prefs = new gadgets.Prefs();
    sourceLinkType = prefs.getString("sourceLinkType");
    direction = prefs.getInt("linkDirection");
    bidirection = prefs.getString("bidirection");

    $("#actionButton").on("click", function () {
      clearSelectedInfo();
      // Blocking the button and starting creation process
      $("#actionButton").attr("disabled", "disabled");
      $(".status").removeClass("warning correct incorrect");
      $(".status").addClass("warning");
      // DODAC KONWERSJE
      $(".status").html(
        `<b>Message:</b> Deleting links for <b>${artRef.length}</b> artifacts.
      <p>Please do not perform any actions until process is finished.</p>
      `
      );
      gadgets.window.adjustHeight();

      if (
        sourceLinkType == null ||
        sourceLinkType == "" ||
        sourceLinkType == "undefined" ||
        sourceLinkType == undefined
      ) {
        clearSelectedInfo();
        $(".status").addClass("incorrect").html("<b>Error:</b> Link type name can not be empty.");
        $("#actionButton").removeAttr("disabled");
      } else {
        coverLinks();
      }
    });
  } else {
    clearSelectedInfo();
    lockAllButtons();
    $(".status")
      .addClass("incorrect")
      .html("<b>Error:</b> The widget can be used only inside IBM Engineering DOORS Next.");
    $("#artifactList").attr("disabled", "disabled");
    gadgets.window.adjustHeight();
  }
});

function lockAllButtons() {
  $("#actionButton").attr("disabled", "disabled");
  $("#actionButton").removeClass("btn-primary");
  $("#actionButton").addClass("btn-secondary");
}

function covertLinkTypes(ref) {
  RM.Data.getLinkedArtifacts(ref[0], [], function (result) {
    console.log(result.code);
    var artLinks = result.data.artifactLinks;
    console.log(artLinks);
    artLinks.forEach(function (linkDefinition) {
      console.log(linkDefinition.linktype + ": " + linkDefinition.targets.length);
    });
  });
}

// Subscribe to the Artifact Selection event
RM.Event.subscribe(RM.Event.ARTIFACT_SELECTED, function (selected) {
  if (selected.length === 1) {
    artRef = selected;
    $("#actionButton").removeAttr("disabled");
    clearSelectedInfo();
    $(".status").html(
      `<b>Message:</b> Press button "Delete links" to change link types for ${selected.length} artifact.`
    );
    gadgets.window.adjustHeight();
  } else if (selected.length > 1) {
    artRef = selected;
    $("#actionButton").removeAttr("disabled");
    clearSelectedInfo();
    $(".status").html(
      `<b>Message:</b> Press button "Delete links" to change link types for ${selected.length} artifact.`
    );
    gadgets.window.adjustHeight();
  } else {
    // clear the display area...
    $("#actionButton").attr("disabled", "disabled");
    clearSelectedInfo();
    // inform the user that they need to select only one thing.
    $(".status").html(
      `<b>Message:</b> Please select artifacts for which You want to delete links and then press "Delete links" button.`
    );
    gadgets.window.adjustHeight();
  }
});

function clearSelectedInfo() {
  $(".status").removeClass("incorrect correct warning");
}
